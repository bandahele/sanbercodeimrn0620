console.log("--BISMILLAHIRROHMANIRROHIIM--")


// Soal No. 1 (Range)
console.log("\n\n--Soal No. 1 (Range)--")

function range(a, b) {
    var arr = []
    if (a <= b) {
        for (a; a <= b; a++) {
            arr.push(a)
        } return arr
    } else if (a > b) {
        for (a; a >= b; a--) {
            arr.push(a)
        } return arr
    } else if (a == 1 || b === undefined) {
        return -1
    }
    else if (a === undefined || b === undefined) {

        return -1
    }
}
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11, 18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 


// Soal No. 2 (Range with Step)
console.log("\n\n\n--Soal No. 2 (Range with Step)--")

function rangeWithStep(a, b, c) {
    var arr2 = []
    if (a < b) {
        for (a; a <= b; a += c) {
            arr2.push(a)
        } return arr2
    } else if (a > b) {
        for (a; a >= b; a -= c) {
            arr2.push(a)
        } return arr2

    }
}
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 


// Soal No. 3 (Sum of Range)
console.log("\n\n\n--Soal No. 3 (Sum of Range)--")

function sum(a, b, c) {
    var total = 0;
    var arr3 = []
    if (a == 1 && b === undefined) {
        return total = 1
    }
    else if (a === undefined && b === undefined) {
        return total = 0
    }
    else if (c === undefined) {
        arr3 = range(a, b)
        for (i = 0; i < arr3.length; i++) {
            total += arr3[i];
        }
    }
    else {
        arr3 = rangeWithStep(a, b, c)
        for (i = 0; i < arr3.length; i++) {
            total += arr3[i];
        }
    } return total


}

console.log(sum(1, 10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15, 10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 


// Soal No. 4 (Array Multidimensi)
console.log("\n\n\n--Soal No. 4 (Array Multidimensi)--")

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]
var identitas = '';
for (var h = 0; h < input.length; h++) {
    identitas = " Nomor ID : " + input[h][0] + "\n Nama Lengkap : " + input[h][1] + "\n TTL: "
        + input[h][2] + " " + input[h][3] + "\n Hobi: " + input[h][4] + "\n "
    console.log(identitas)
}


// Soal No. 5 (Balik Kata)
console.log("\n\n\n--Soal No. 5 (Balik Kata)--")

function balikKata(str) {

    return (str.split('').reverse().join(''));
}
console.log(balikKata('Kasur Rusak')) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 


// Soal No. 6 (Metode Array)
console.log("\n\n\n--Soal No. 6 (Metode Array)--")

var input1 = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]
function dataHandling2(input1) {

    input1.splice(1, 2, "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung");
    input1.splice(4, 1, "Pria", "SMA Internasional Metro");
    console.log(input1)


    // ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"] 
    var datesplit = input1[3].split("/");
    switch (datesplit[1]) {
        case '01': { console.log('januari'); break; }
        case '02': { console.log('februari'); break; }
        case '03': { console.log('maret'); break; }
        case '04': { console.log('april'); break; }
        case '05': { console.log('mei'); break; }
        case '06': { console.log('juni'); break; }
        case '07': { console.log('juli'); break; }
        case '08': { console.log('agustus'); break; }
        case '09': { console.log('september'); break; }
        case '10': { console.log('oktober'); break; }
        case '11': { console.log('november'); break; }
        case '12': { console.log('desember'); break; }
    }
console.log(datesplit.sort((a, b) => b - a));
console.log(input1[3].split("/").join('-'));
console.log(input1[1].slice(0,15));

}


dataHandling2(input1);

console.log("\n\n\nALHAMDULILLAH... Selesai pukul 00:07 WIB... Ngantookkkkkkkk  :)")