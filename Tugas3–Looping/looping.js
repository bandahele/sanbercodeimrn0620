// No. 1 Looping While
console.log('\n No. 1 Looping While')
console.log('==LOOPING PERTAMA==');

var urut = 2;
while (urut <= 20) { // Loop akan terus berjalan selama nilai urut di bawah atau samadengan 20
    console.log(urut + ' - I love coding'); // Menampilkan nilai urut
    urut += 2; // Mengubah nilai urut dengan menambahkan 2
}

console.log('\n==LOOPING KEDUA==');
var urut = 20;
while (urut >= 2) { // Loop akan terus berjalan selama nilai urut di atas atau samadengan 2
    console.log(urut + ' - I will become a mobile developer'); // Menampilkan nilai urut
    urut -= 2; // Mengubah nilai urut dengan pengurangan 2
}



// No. 2 Looping menggunakan for
console.log('\n No. 2 Looping menggunakan for')
for (var angka = 1; angka <= 20; angka++) {
    if (angka % 2 !== 0 && angka % 3 !== 0) { console.log(angka + ' Santai') }
    else if (angka % 2 == 0) { console.log(angka + ' Berkualitas') }
    else if (angka % 3 == 0) { console.log(angka + ' I Love Coding') }

}



//   No. 3 Membuat Persegi Panjang #
console.log('\n No. 3 Membuat Persegi Panjang #')

console.log(' ALternatif I');
var panjang = '';
for (var i = 0; i < 4; i++) {
    for (var j = 0; j < 8; j++) {
        panjang += '#';
    }
    panjang += '\n'
}
console.log(panjang)

console.log('\n ALternatif II');
var persegi = "####";
for (const char of persegi) {
    console.log(char.repeat(8));
}




// //   No. 4 Membuat Tangga 
console.log('\n No. 4 Membuat Tangga ')

console.log('\n Alternatif I ')
var pagar = '#';
while (pagar.length <= 7) {
    console.log(pagar);
    pagar += '#';
}

console.log('\n Alternatif II ')
var tangga = '';
var tinggi = 7;
for (var v = 0; v < tinggi; v++) {
    for (var h = 0; h <= v; h++) {
        tangga += '#'
    }
    tangga +='\n'
}

console.log(tangga);




// No. 5 Membuat Papan Catur
console.log('\n No. 5 Membuat papan Catur')

var hitam = "#"
var putih = " ";
var Catur = " ";
var lebar = 8;
for (var i = 0; i < lebar; i++) {

    for (var j = 0; j < lebar; j++) {
        if (i % 2 == 1) {

            if (j % 2 == 0) {
                Catur = Catur + hitam;
            }
            else {
                Catur = Catur + putih;
            }

        }
        else {
            if (j % 2 == 0) {
                Catur = Catur + putih;
            }
            else {
                Catur = Catur + hitam;
            }
        }
    }

    console.log(Catur);
    Catur = " ";
}