//Soal No. 1 (Membuat kalimat)
console.log('===== Soal No. 1 (Membuat kalimat)====='); 
var word = 'JavaScript'; 
var second = 'is'; 
var third = 'awesome'; 
var fourth = 'and'; 
var fifth = 'I'; 
var sixth = 'love'; 
var seventh = 'it!';

console.log ( word + ' ' +second + ' ' + third + ' ' + fourth + ' ' + fifth + ' ' + sixth + ' ' + seventh )


console.log('\n===== Soal No.2 Mengurai kalimat (Akses karakter dalam string),====='); 
//Soal No.2 Mengurai kalimat (Akses karakter dalam string),
var sentence = "I am going to be React Native Developer"; 
var FirstWord = sentence[0] ; 
var SecondWord  = sentence[2] + sentence[3]  ; 
var thirdWord   = sentence[5] + sentence[6] + sentence[7] + sentence[8] + sentence[9]  ; // lakukan sendiri 
var fourthWord  = sentence[11] + sentence[12]  ; // lakukan sendiri 
var fiftWord    = sentence[14] + sentence[15]  ; // lakukan sendiri 
var sixthWord   = sentence[17] + sentence[18] + sentence[19] + sentence[20] + sentence[21] ; // lakukan sendiri 
var seventhWord = sentence[30] + sentence[31] + sentence[32] + sentence[33] + sentence[34] + sentence[35] + sentence[36] + sentence[37] + sentence[38]   ; // lakukan sendiri 
console.log('First Word: ' + FirstWord); 
console.log('Second Word: ' + SecondWord); 
console.log('Third Word: ' + thirdWord); 
console.log('Fourth Word: ' + fourthWord); 
console.log('Fifth Word: ' + fiftWord); 
console.log('Sixth Word: ' + sixthWord); 
console.log('Seventh Word: ' + seventhWord); 

console.log('=====Alternatif Jawab No 2====='); 
//  Alternatif Jawab No 2
var words = sentence.split(' ');
var word0 = words[0];
var word1 = words[1];
var word2 = words[2];
var word3 = words[3];
var word4 = words[4];
var word5 = words[5];
var word6 = words[6];
var word7 = words[7];
console.log('First Word: ' + word0); 
console.log('Second Word: ' + word1); 
console.log('Third Word: ' + word2); 
console.log('Fourth Word: ' + word3); 
console.log('Fifth Word: ' + word4); 
console.log('sixth Word: ' + word5); 
console.log('seventh Word: ' + word7); 

// Soal No. 3 Mengurai Kalimat (Substring)
console.log('\n\n===== Soal No. 3 Mengurai Kalimat (Substring)======'); 

var sentence2   = 'wow JavaScript is so cool'; 
var FirstWord2  = sentence2.substring(0, 3); 
var secondWord2 = sentence2.substring(4, 14); // do your own! 
var thirdWord2  = sentence2.substring(15, 17); // do your own! 
var fourthWord2 = sentence2.substring(18, 20); // do your own! 
var fifthWord2  = sentence2.substring(21, 25); // do your own! 
console.log('First Word: ' + FirstWord2); 
console.log('Second Word: ' + secondWord2); 
console.log('Third Word: ' + thirdWord2); 
console.log('Fourth Word: ' + fourthWord2); 
console.log('Fifth Word: ' + fifthWord2);
 


// Soal No. 4 Mengurai Kalimat dan Menentukan Panjang String
console.log('\n\n===== Soal No. 4 Mengurai Kalimat dan Menentukan Panjang String====='); 

var sentence3   = 'wow JavaScript is so cool'; 
var FirstWord3  = sentence3.substring(0, 3); 
var secondWord3 = sentence3.substring(4, 14); // do your own! 
var thirdWord3  = sentence3.substring(15, 17); // do your own! 
var fourthWord3 = sentence3.substring(18, 20); // do your own! 
var fifthWord3  = sentence3.substring(21, 25); // do your own! 

var firstWordLength     = FirstWord3.length;
var secondWordLength    = secondWord3.length;
var thirdWordLength     = thirdWord3.length;
var fourthWordLength    = fourthWord3.length;
var fifthWordLength     = fifthWord3.length;



// lanjutkan buat variable lagi di bawah ini 
console.log('First Word: ' + FirstWord3     + ', with length: ' + firstWordLength); 
console.log('Second Word: ' + secondWord3   + ', with length: ' + secondWordLength); 
console.log('Third Word: ' + thirdWord3     + ', with length: ' + thirdWordLength); 
console.log('Fourth Word: ' + fourthWord3   + ', with length: ' + fourthWordLength); 
console.log('Fifth Word: ' + fifthWord3     + ', with length: ' + fifthWordLength); 