//Tugas Conditional
console.log('===== Tugas Conditional- IF ELSE=====');


var nama = "Ribas"
var peran = "werewolf"

//Mengecek Isian nama dan Peran
if (nama == "") {
    console.log("Nama harus diisi!")
} else if (peran == "") {
    console.log("Halo " + nama + ", Pilih peranmu untuk memulai game!")
}

//Conditional untuk Jenis Peran
else if (peran == "penyihir") {
    console.log("Selamat datang di Dunia Werewolf, " + nama +
        "\nHalo Penyihir " + nama + ", kamu dapat melihat siapa yang menjadi werewolf!")
}
else if (peran == "guard") {
    console.log("Selamat datang di Dunia Werewolf, " + nama +
        "\nHalo Guard " + nama + ", kamu akan membantu melindungi temanmu dari serangan werewolf.")
}
else if (peran == "werewolf") {
    console.log("Selamat datang di Dunia Werewolf, " + nama +
        "\nHalo Werewolf  " + nama + ", Kamu akan memakan mangsa setiap malam!")
}



// Tugas Conditional- Switch Case
console.log('\n\n===== Tugas Conditional- Switch Case====');

var tanggal = 9; // assign nilai variabel tanggal disini! (dengan angka antara 1 - 31)
var bulan = 9; // assign nilai variabel bulan disini! (dengan angka antara 1 - 12)
var tahun = 1988; // assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)

if (tanggal == undefined || bulan == undefined || bulan == null || bulan == '' || tahun == undefined) { console.log("Silahkan isian di lengkapi!") }
else if (tanggal > 31 || tanggal < 1) { console.log("Anda salah mengisi Tanggal (hanya angka 1 sampai 31!") }
else if (bulan > 12 || bulan < 1) { console.log("Anda salah mengisi bulan (hanya angka 1 sampai 12!") }
else if (tahun > 2200 || tahun < 1900) { console.log("Anda salah mengisi tahun (hanya angka 1900 sampai 2200!") }
else
    switch (bulan) {
        case 1: { console.log(tanggal + ' Januari ' + tahun); break; }
        case 2: { console.log(tanggal + ' Februari ' + tahun); break; }
        case 3: { console.log(tanggal + ' Maret ' + tahun); break; }
        case 4: { console.log(tanggal + ' April ' + tahun); break; }
        case 5: { console.log(tanggal + ' Mei ' + tahun); break; }
        case 6: { console.log(tanggal + ' Juni ' + tahun); break; }
        case 7: { console.log(tanggal + ' Juli ' + tahun); break; }
        case 8: { console.log(tanggal + ' Agustus ' + tahun); break; }
        case 9: { console.log(tanggal + ' September ' + tahun); break; }
        case 10: { console.log(tanggal + ' Oktober ' + tahun); break; }
        case 11: { console.log(tanggal + ' November ' + tahun); break; }
        case 12: { console.log(tanggal + ' Desember ' + tahun); break; }
        default: { console.log('Tidak terjadi apa-apa'); }
    }



