import React from 'react';
import { MaterialIcons } from '@expo/vector-icons';
import { Entypo } from '@expo/vector-icons';
import {
    StyleSheet,
    Text,
    View,
    TextInput,
    TouchableOpacity,
    Image,
    ImageBackground
} from 'react-native';



const image = { uri: "https://reactjs.org/logo-og.png" };

export default function LoginScreen({ navigation }) {

    return (

        <View style={styles.container}>

            <ImageBackground source={image} style={styles.image}>
            <Image source={require('./assets/logo.png')} style={{ width: 300, height: 100, marginTop: -100 }} />
            </ImageBackground>
            <Text style={{ marginTop: 100, fontSize: 25, fontWeight: 'bold' }}>:: Log In ::</Text>
           
            <View style={styles.inputText}>
                <MaterialIcons style={{ marginRight: 5 }} name="email" size={24} color="black" />
                <TextInput placeholder="Masukkan email" style={{ fontSize: 13 }}></TextInput>
            </View>

            <View style={styles.inputText}>
                <Entypo style={{ marginRight: 5 }} name="lock" size={24} color="black" />
                <TextInput placeholder="Masukkan Password" style={{ fontSize: 13 }}></TextInput>
            </View>

            <TouchableOpacity style={styles.buttonMasuk} onPress={() => navigation.push('DrawerScreen')}>
                <Text style={{ backgroundColor: "#3EC6FF", color: "white", paddingHorizontal: 25, paddingVertical: 10, borderRadius: 100 }}>Masuk</Text>
            </TouchableOpacity>
            <View style={{ marginTop: 15 }}>
                <Text>Atau</Text>
            </View>

            <TouchableOpacity style={styles.buttonDaftar}>
                <Text style={{ backgroundColor: "#003366", color: "white", paddingHorizontal: 25, paddingVertical: 10, borderRadius: 200 }}>Daftar ?</Text>
            </TouchableOpacity>


            <View style={styles.footer}>
                <Text style={{ fontWeight: 'bold' }}>About Us @bandahele</Text>
                {/* <Button title="Drawer" onPress={() => navigation.openDrawer()} /> */}
            </View>

            
        </View>

    );

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
        display: 'flex',
    },
    text: {
        fontSize: 30, color: '#04063c'
    },
    inputText: {
        marginTop: 20,
        width: 200,
        height: 30,
        paddingHorizontal: 10,
        borderBottomWidth: 1,
        borderColor: "#003366",
        flexDirection: "row"
    },
    buttonMasuk: {
        marginTop: 20,
        height: 30,
        alignItems: 'center',
    },
    buttonDaftar: {
        marginTop: 15,
        height: 30,
        alignItems: 'center',
    },
    footer: {
        marginTop: 70,
        height: 30,
        backgroundColor: 'white',
        elevation: 3,
        paddingHorizontal: 15,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    image: {
        flex: 0,
        height: 100,
        resizeMode: "cover",
        justifyContent: "center"
    },
    text: {
        color: "grey",
        fontSize: 30,
        fontWeight: "bold"

    }
});
