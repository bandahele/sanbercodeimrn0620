import React, { useState } from "react";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator, BottomTabBar } from '@react-navigation/bottom-tabs'
import { createDrawerNavigator } from '@react-navigation/drawer'
import AboutScreen from './AboutScreen'
import AddScreen from './AddScreen'
import LoginScreen from './LoginScreen'
import ProjectScreen from './ProjectScreen'
import SkillScreen from './SkillScreen'

const Stack = createStackNavigator()
const Tabs = createBottomTabNavigator()
//const Drawer = createDrawerNavigator()

const TabsScreen = () => (
    <Tabs.Navigator  >

    <Tabs.Screen name="Skill  Screen" component={SkillScreen} />
    <Tabs.Screen name="Project Screen"  component={ProjectScreen} />
    <Tabs.Screen name="Add  Screen" component={AddScreen} />
    </Tabs.Navigator >
);


const Drawer = createDrawerNavigator();
const DrawerScreen = () => (
    <Drawer.Navigator initialRouteName="Profile">
        <Drawer.Screen name="Home" component={TabsScreen} />
        <Drawer.Screen name="About Us" component={AboutScreen} />
    </Drawer.Navigator>

    // const DrawerScreen = () => (
    //     <Drawer.Navigator screenOptions={{header: () => null}}>

    //         <Drawer.Screen name="Home" component={TabsScreen}  />
    //         <Drawer.Screen name="About Us" component={AboutScreen} />
    //     </Drawer.Navigator>
);



export default function App() {
    return (
        <NavigationContainer>
            <Stack.Navigator initialRouteName="Login Screen">
                <Stack.Screen name="LoginScreen" component={LoginScreen} />
                <Stack.Screen name="DrawerScreen" component={DrawerScreen} />
            </Stack.Navigator>
        </NavigationContainer>
    )
}