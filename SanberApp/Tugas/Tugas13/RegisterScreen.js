import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  ScrollView,
} from "react-native";


export default class App extends React.Component {
  render() {
    return (
      <ScrollView>
        <View style={styles.container}>
          <View style={styles.row}>
            <Image
              source={require("./assets/logo.png")}
              style={{ height: 100, width: 350, marginHorizontal: 30 }}
            />
          </View>
          <Text style={styles.header}>DAFTAR</Text>

          <View style={styles.inputRow}>
            <Text style={{ fontSize: 16, color: "#003366" }}>Username</Text>
            <TextInput style={styles.inputBox}></TextInput>
            <Text style={{ fontSize: 16, color: "#003366" }}>Email</Text>
            <TextInput style={styles.inputBox}></TextInput>
            <Text style={{ fontSize: 16, color: "#003366" }}>Password</Text>
            <TextInput style={styles.inputBox}></TextInput>
            <Text style={{ fontSize: 16, color: "#003366" }}>
              Ulangi Password
            </Text>
            <TextInput style={styles.inputBox}></TextInput>
          </View>

          <TouchableOpacity style={styles.daftarButton}>
            <Text style={styles.buttonText}>Daftar</Text>
          </TouchableOpacity>

          <Text style={{ color: "#3EC6FF", fontSize: 24 }}>Sudah Terdaftar ?</Text>
          <TouchableOpacity style={styles.masukButton}>
            <Text style={styles.buttonText}>Masuk</Text>
          </TouchableOpacity>a
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    justifyContent: "center",
    alignItems: "center",
    marginVertical: 50,
  },
  header: {
    paddingVertical: 30,
    fontSize: 24,
    color: "#003366",
  },
  inputRow: {
    paddingLeft: 30,
    paddingRight: 30,
  },
  inputBox: {
    width: 290,
    padding: 10,
    borderWidth: 1,
    borderColor: "#003366",
    paddingHorizontal: 16,
    fontSize: 16,
    color: "#000",
    marginVertical: 10,
  },
  daftarButton: {
    width: 150,
    backgroundColor: "#003366",
    borderRadius: 30,
    marginTop: 20,
    marginBottom: 40,
    paddingVertical: 20,
  },
  masukButton: {
    width: 140,
    backgroundColor: "#3EC6FF",
    borderRadius: 16,
    marginTop: 5,
    marginBottom: 10,
    paddingVertical: 20,
  },

  buttonText: {
    fontSize: 24,
    color: "#ffffff",
    textAlign: "center",
  },
});
