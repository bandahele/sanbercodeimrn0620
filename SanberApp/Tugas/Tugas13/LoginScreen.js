import React from "react";
import {

    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    TouchableOpacity,
    ScrollView,
} from "react-native";
import Icon from 'react-native-vector-icons/FontAwesome';
// import { Input } from 'react-native-elements';

export default class App extends React.Component {
    render() {
        return (
            <ScrollView>
                <View style={styles.container}>
                    <View style={styles.row}>
                        <Image
                            source={require("./assets/logo.png")}
                            style={{ height: 100, width: 350, marginHorizontal: 30 }}
                        />
                    </View>
                    <Text style={styles.header}>LOGIN</Text>

                    <View style={styles.inputRow}>
                        <Text style={{ fontSize: 16, color: "#003366" }}> Username / Email </Text>
                        <TextInput style={styles.inputBox}></TextInput>

                        <Text style={{ fontSize: 16, color: "#003366" }}>Password</Text>
                        <TextInput style={styles.inputBox}></TextInput>
                    </View>

                    <TouchableOpacity style={styles.masukButton}>
                        <Text style={styles.buttonText}>Masuk</Text>
                    </TouchableOpacity>

                    <Text style={{ color: "#3EC6FF", fontSize: 24 }}>atau</Text>
                    <TouchableOpacity style={styles.daftarButton}>
                        <Text style={styles.buttonText}>Daftar ?</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flexGrow: 1,
        justifyContent: "center",
        alignItems: "center",
        marginVertical: 50,
    },
    textInput: {
        width: '100%',
        paddingVertical: 0,
        paddingHorizontal: 15,
        height: 40,
        margin: 0,
        fontSize: 18,
        borderWidth: 1,
        borderColor: 'rgba(0,0,0,0.5)',
        backgroundColor: 'white'
    },
    header: {
        paddingVertical: 30,
        fontSize: 24,
        color: "#003366",
    },
    inputRow: {
        paddingLeft: 10,
        paddingRight: 10,
    },
    inputBox: {
        width: 350,
        padding: 10,
        borderWidth: 2,
        borderColor: "#003366",
        paddingHorizontal: 16,
        fontSize: 16,
        color: "#000",
        marginVertical: 10,
    },
    daftarButton: {
        width: 140,
        backgroundColor: "#003366",
        borderRadius: 16,
        marginTop: 10,
        paddingVertical: 20,
    },
    masukButton: {
        width: 140,
        backgroundColor: "#3EC6FF",
        borderRadius: 16,
        marginTop: 30,
        marginBottom: 10,
        paddingVertical: 20,
    },

    buttonText: {
        fontSize: 24,
        color: "#ffffff",
        textAlign: "center",
    },
});
