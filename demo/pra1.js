// // class Car {
// //     constructor(brand) {
// //       this.carname = brand;
// //     }
// //     present() {
// //       return "I have a " + this.carname;
// //     }
// //   }
  
// //   mycar = new Car("Ford");
// //   console.log(mycar.present()) // I have a Ford

// //   class Car {
// //     constructor(brand) {
// //       this.carname = brand;
// //     }
// //     present(x) {
// //       return x + ", I have a " + this.carname;
// //     }
// //   }
  
// //   mycar = new Car("Ford");
// //   console.log(mycar.present("Hello"));


//   class Car {
//     constructor(brand) {
//       this.carname = brand;
//     }
//     static hello() {
//       return "Hello!!";
//     }
//   }
  
//   mycar = new Car("Ford");
  
//   // memanggil 'hello()' pada class Car:
//   console.log(mycar.hello());
  
//   // dan tidak bisa pada 'mycar':
//   // console.log(mycar.hello());
//   // jika menggunakan sintaks tersebut akan memunculkan error.


  class Car {
    constructor(brand) {
      this.carname = brand;
    }
    present() {
      return 'I have a ' + this.carname;
    }
  }
  
  class Model extends Car {
    constructor(brand, mod) {
      super(brand);
      this.model = mod;
    }
    show() {
      return this.present() + ', it is a ' + this.model;
    }
  }
  
  mycar = new Model("Ford", "Mustang");
  console.log(mycar.show());