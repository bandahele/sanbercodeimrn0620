console.log('====Tugas 1====')

function findAge(tahun) {

    var now = new Date()
    var thisYear = now.getFullYear() // 2020 (tahun sekarang)
    var umur = thisYear - tahun;
    if (tahun == undefined || umur <1) {
        return "Invalid birth year";
    } else { return umur }
}

function arrayToObject(arr) {
    if (arr == '') {
        console.log('');
    } else {
        for (var i = 0; i < arr.length; i++) {
            var obj = {
                firstName: arr[i][0],
                lastName: arr[i][1],
                gender: arr[i][2],
                age: findAge(arr[i][3])
            }
            console.log(i + 1 + '.' + obj.firstName + ' ' + obj.lastName, obj);
        }
    }
}

// Driver Code
var people = [["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"]]
arrayToObject(people)
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

var people2 = [["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023]]
arrayToObject(people2)
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

// Error case 
arrayToObject([]) // ""



// Soal No. 2 (Shopping Time)
console.log('\n\n=====Soal No. 2 (Shopping Time)=====')

function shoppingTime(memberId = '', money = 0) {
    var hasil

    if (memberId == undefined || money == undefined || memberId == '') {
        return "Mohon maaf, toko X hanya berlaku untuk member saja";
    }
    else if (money < 50000) {
        return 'Mohon maaf, uang tidak cukup'
    }
    else {
        var sale = [
            ["Sepatu brand Stacattu", 1500000],
            ["Baju brand Zoro", 500000],
            ["Baju brand H&N", 250000],
            ["Sweater brand Uniklooh", 175000],
            ["Casing Handphone", 50000]
        ];
        var sisa = money;
        var list = []
        for (let x = 0; x < sale.length; x++) {
            if (sisa >= sale[x][1]) {
                sisa -= sale[x][1]
                list.push(sale[x][0])
            }
        }


        hasil = {
            memberId: memberId,
            money: money,
            listPurchased: list,
            changeMoney: sisa
        }

        return hasil
    }
}


// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja


// Soal No. 3 (Naik Angkot)
console.log('\n\n=====Soal No. 3 (Naik Angkot)=====')
function naikAngkot(arrPenumpang) {
    var rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    rute = rute.sort(function (a, b) { return a < b ? -1 : 1 })

    var output = [];
    var naik = 0;
    var tujuan = 0;
    var indexNaik = 0;
    var indexTujuan = 0;
    var totalPrice = 0;

    if (arrPenumpang !== undefined || arrPenumpang.length != 0) {
        for (let x = 0; x < arrPenumpang.length; x++) {
            naik = arrPenumpang[x][1];
            tujuan = arrPenumpang[x][2];

            for (let index = 0; index < rute.length; index++) {
                if (naik == rute[index]) {
                    indexNaik = index + 1;
                }
                if (tujuan == rute[index]) {
                    indexTujuan = index + 1;
                }
            }
            var listAngkot = {};
            totalPrice = (indexTujuan - indexNaik) * 2000;
            listAngkot.penumpang = arrPenumpang[x][0];
            listAngkot.naikDari = naik;
            listAngkot.tujuan = tujuan;
            listAngkot.bayar = totalPrice
            output.push(listAngkot);
        }
    }
    return output
}

//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));

