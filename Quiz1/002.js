/*

  A. Descending Ten (10 poin)
    Function DescendingTen adalah kebalikan dari function AscendingTen. 
    Output yang diharapkan adalah deretan angka dimulai dari angka parameter hingga 10 angka di bawahnya. 
    Function akan mengembalikan nilai -1 jika tidak ada parameter yang diberikan.

    contoh: 
    console.log(DescendingTen(10)) akan menampilkan 10 9 8 7 6 5 4 3 2 1
    console.log(DescendingTen(20)) akan menampilkan 20 19 18 17 16 15 14 13 12 11

    Hint: Deretan angka yang menjadi output adalah dalam tipe data String, bukan Number.
  
  B. Ascending Ten (10 poin)

    Buatlah sebuah function dengan nama AscendingTen yang menerima sebuah parameter berupa Number, 
    function AscendingTen tersebut akan mengembalikan deretan angka yang ditampilkan 
    dalam satu baris (ke samping). Deret angka yang ditampilkan adalah deretan angka 
    mulai dari angka yang menjadi parameter input function hingga 10 angka setelahnya yang dipisah dengan karakter spasi. 
    Function akan mengembalikan nilai -1 jika tidak ada parameter yang diberikan.
    contoh: 
    console.log(AscendingTen(1)) akan menampilkan 1 2 3 4 5 6 7 8 9 10  
    console.log(Ascending(101)) akan menampilkan 101 102 103 104 105 106 107 108 109 110

    Hint: Deretan angka yang menjadi output adalah dalam tipe data String, bukan Number.

  C. Conditional Ascending Descending (15)

    Tulislah sebuah function ConditionalAscDesc yang menerima dua buah parameter dengan tipe Number. 
    Parameter number pertama diberi nama reference, dan parameter number kedua diberi nama check. 
    Function ini mirip seperti kedua function sebelumnya yaitu AscendingTen 
    dan DescendingTen yaitu akan menampilkan 10 angka berderet dimulai atau diakhiri dari reference. 
    Function ConditionalAscDesc mengecek jika parameter check merupakan ganjil 
    maka output yang ditampilkan yaitu deretan angka ascending, 
    jika parameter check merupakan angka genap maka output yang ditampilkan yaitu deretan angka descending. 
    Function akan mengembalikan nilai -1 jika tidak ada parameter yang diberikan atau hanya satu saja parameter yang diberikan.

    Contoh: 
    console.log(ConditionalAscDesc(1, 1)) akan menampilkan 1 2 3 4 5 6 7 8 9 10
    console.log(ConditionalAscDesc(100, 4)) akan menampilkan 100 99 98 97 96 95 94 93 92 91

  D. Papan Ular Tangga (35)
    Buatlah sebuah function ularTangga yang ketika function tersebut dipanggil akan menampilkan papan ular tangga ukuran 10 x 10. 

    Output: 
    100 99 98 97 96 95 94 93 92 91
    81 82 83 84 85 86 87 88 89 90
    80 79 78 77 76 75 74 73 72 71
    61 62 63 64 65 66 67 68 69 70
    60 59 58 57 56 55 54 53 52 51
    41 42 43 44 45 46 47 48 49 50
    40 39 38 37 36 35 34 33 32 31
    21 22 23 24 25 26 27 28 29 30
    20 19 18 17 16 15 14 13 12 11
    1 2 3 4 5 6 7 8 9 10

*/

function DescendingTen(num) {
  if (num === undefined) { return '-1' } else {
    for (var i = num; i > (num - 10); i--) {
      {
        console.log(i.toString())
      }
    }
  }
}


function AscendingTen(num) {
  var ascen = '';
  // for (var i = 1; i < 3; i += 1) {
  if (num === undefined) { ascen = '-1' } else {

    for (var k = num; k < (num + 10); k++) {
      ascen = ascen + k + " ";
    }
  }
  console.log(ascen.toString());
}








function ConditionalAscDesc(reference, check) {
  var i = '';
  if (check % 2 == 0) {
    for (var i = reference; i > (reference - 10); i--) {
      console.log(i.toString())
    }
  }
  else if (check % 2 == 1) {
    for (var i = reference; i < (reference + 10); i++) {

      console.log(i.toString())
    }

  }
  else if (check == undefined) { return '-1' }
  else if (check == undefined && reference == undefined) { return '-1' }


}

// Tulis code kamu di sini











function ularTangga() {
  var hasil1 = '';
  var hasil2 = '';
  var hasil3 = '';
  var hasil4 = '';
  var hasil5 = '';
  var hasil6 = '';
  var hasil7 = '';
  var hasil8 = '';
  var hasil9 = '';
  var hasil10 = '';
  for (var e = 1; e <= 10; e += 1) {
    //console.log(e);
    if (e === 1) {
      for (var j = 100; j >= 91; j -= 1) {
        hasil1 = hasil1 + j + " ";

      }
      console.log(hasil1);
    } else if
      (e === 2) {
      for (var j = 81; j <= 90; j += 1) {
        hasil2 = hasil2 + j + " ";

      }
      console.log(hasil2);
    } else if
      (e === 3) {
      for (var j = 80; j >= 71; j -= 1) {
        hasil3 = hasil3 + j + " ";

      }
      console.log(hasil3);

    } else if
      (e === 4) {
      for (var j = 61; j <= 70; j += 1) {
        hasil4 = hasil4 + j + " ";

      }
      console.log(hasil4);
    } else if
      (e === 5) {
      for (var j = 60; j >= 51; j -= 1) {
        hasil5 = hasil5 + j + " ";

      }
      console.log(hasil5);
    } else if
      (e === 6) {
      for (var j = 41; j <= 50; j += 1) {
        hasil6 = hasil6 + j + " ";

      }
      console.log(hasil6);
    } else if
      (e === 7) {
      for (var j = 40; j >= 31; j -= 1) {
        hasil7 = hasil7 + j + " ";

      }
      console.log(hasil7);
    } else if
      (e === 8) {
      for (var j = 21; j <= 30; j += 1) {
        hasil8 = hasil8 + j + " ";

      }
      console.log(hasil8);
    } else if
      (e === 9) {
      for (var j = 20; j >= 11; j -= 1) {
        hasil9 = hasil9 + j + " ";

      }
      console.log(hasil9);
    } else if
      (e === 10) {
      for (var j = 1; j <= 10; j += 1) {
        hasil10 = hasil10 + j + " ";

      }
      console.log(hasil10);
    }
  }
}


// TEST CASES Descending Ten
console.log("\n-----TEST CASES Descending Ten-----\n")
console.log(DescendingTen(100)) // 100 99 98 97 96 95 94 93 92 91
console.log(DescendingTen(10)) // 10 9 8 7 6 5 4 3 2 1
console.log(DescendingTen()) // -1

// TEST CASES Ascending Ten
console.log("\n\n-------TEST CASES Ascending Ten-------\n\n")
console.log(AscendingTen(11)) // 11 12 13 14 15 16 17 18 19 20
console.log(AscendingTen(21)) // 21 22 23 24 25 26 27 28 29 30
console.log(AscendingTen()) // -1

// // TEST CASES Conditional Ascending Descending
console.log("\n-----TEST CASES Conditional Ascending Descending-----\n")
console.log(ConditionalAscDesc(20, 8)) // 20 19 18 17 16 15 14 13 12 11
console.log(ConditionalAscDesc(81, 1)) // 81 82 83 84 85 86 87 88 89 90
console.log(ConditionalAscDesc(31)) // -1
console.log(ConditionalAscDesc()) // -1

// TEST CASE Ular Tangga
console.log("\n\n-----TEST CASE Ular Tangga-----\n\n")
console.log(ularTangga())
/*
// Output :
//   100 99 98 97 96 95 94 93 92 91
//   81 82 83 84 85 86 87 88 89 90
//   80 79 78 77 76 75 74 73 72 71
//   61 62 63 64 65 66 67 68 69 70
//   60 59 58 57 56 55 54 53 52 51
//   41 42 43 44 45 46 47 48 49 50
//   40 39 38 37 36 35 34 33 32 31
//   21 22 23 24 25 26 27 28 29 30
//   20 19 18 17 16 15 14 13 12 11
//   1 2 3 4 5 6 7 8 9 10
// */
