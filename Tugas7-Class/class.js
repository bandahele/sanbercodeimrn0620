// 1. Animal Class 
console.log('\n\n====Tugas 1====')
console.log('Release 0')

class Animal {
    constructor(name) {
        this._name = name;
        this._legs = 4;
        this._cold_blooded = false;
    }
    get name() {
        return this._name;
    }
    get legs() {
        return this._legs;
    }
    get cold_blooded() {
        return this._cold_blooded;
    }

}
var sheep = new Animal("shaun");
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false



//soal no 1 release 1
console.log('\n\n=====Release 1=====')

class Ape extends Animal {
    constructor(name) {
        super(name)
        this._leg = 2
        this._sound = "Auooo"
    }
    yell() {
        console.log(this._sound)
    }
}


class Frog extends Animal {
    constructor(name) {
        super(name)
        this._leg = 2
        this._sound = 'Hop Hop'
    }
    jump() {
        console.log(this._sound)
    }
}


// Code class Ape dan class Frog di sini

var sungokong = new Ape("kera Sakti")
sungokong.yell() // "Auooo"

var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 


//soal 2. Function to Class
console.log('\n\n======2. Function to Class======')
class Clock {

    constructor({template})
    {
    	this.template = template;
    }

    render()
    {
    	var date = new Date();

	    var hours = date.getHours();
	    if (hours < 10) hours = '0' + hours;

	    var mins = date.getMinutes();
	    if (mins < 10) mins = '0' + mins;

	    var secs = date.getSeconds();
	    if (secs < 10) secs = '0' + secs;

	    var output = this.template
	      .replace('h', hours)
	      .replace('m', mins)
	      .replace('s', secs);

	    console.log(output);
    }

    start()
    {
    	this.timer = setInterval(this.render.bind(this), 1000)
    }

    stop()
    {
    	clearInterval(this.timer);
    }
}

var clock = new Clock({template: 'h:m:s'});
clock.start();  
