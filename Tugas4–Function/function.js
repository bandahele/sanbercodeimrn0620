//  No. 1
console.log("\n=== No. 1 ===");

function teriak() {
    return "Halo Sanbers!"
}

console.log(teriak());


//  No. 2
console.log("\n=== No. 2 ===");


function multiply(num1, num2) {
    return num1 * num2
};
var num1 = 12
var num2 = 4
var hasilKali = multiply(num1, num2)
console.log(hasilKali) // 48


//  No. 3
console.log("\n=== No. 3 ===");

function introduce1(name, age, address, hobby) {
    return "Nama saya " + name + ", umur saya " + age + " tahun, alamat saya di " + address + ", dan saya punya hobby yaitu " + hobby;
}
var name = 'Agus';
var age = '30';
var address = "Jln. Malioboro, Yogyakarta";
var hobby = "Gaming"
var perkenalan = introduce1(name, age, address, hobby);

console.log(perkenalan) 

// THE END