// 1. Mengubah fungsi menjadi fungsi arrow
console.log("===1. Mengubah fungsi menjadi fungsi arrow===")
console.log("==ES6==")


const golden = () => { console.log("This is Golden") }
golden()


// 2. Sederhanakan menjadi Object literal di ES6
console.log("\n\n=====================2. Sederhanakan menjadi Object literal di ES6============")
console.log("==ES6==")

const nama = (a, b) => {
    return {
        fullname: () => { console.log(`${a} ${b}`) }

    }
}
nama("William", "Imoh").fullname()



// 3. Destructuring
console.log("\n\n===========================3. Destructuring=====================")
console.log("==ES6==")

const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}

const { firstName, lastName, destination, occupation } = newObject;
console.log(firstName, lastName, destination, occupation)


// 4. Array Spreading
console.log("\n\n=================4. Array Spreading=======================")
console.log("==ES6==")


const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]


let combined = [...west, ...east]
console.log(combined)

// 5. Template Literals
console.log("\n\n=================5. Template Literals======================")
console.log("==ES6==")

const planet = "earth"
const view = "glass"

const before = `Lorem ${view} dolor sit amet, 
consectetur adipiscing elit, ${planet} do eiusmod tempor 
incididunt ut labore et dolore magna aliqua. Ut enim 
ad minim veniam`
console.log(before)